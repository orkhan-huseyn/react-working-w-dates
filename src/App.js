import { useState } from 'react'
import { Form, Button } from 'react-bootstrap'
import Flatpickr from 'react-flatpickr'
import moment from 'moment'

import 'flatpickr/dist/themes/airbnb.css'

function App() {
  const [name, setName] = useState('')
  const [surname, setSurname] = useState('')
  const [birthDate, setBirthdate] = useState('')

  function handleFormSubmit(event) {
    event.preventDefault()
    const requestBody = {
      name,
      surname,
      birthDate: moment(birthDate).format('YYYY-MM-DD'),
    }

    fetch('http://localhost:8080/users', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(requestBody),
    })
      .then((response) => response.json())
      .then((response) => {
        console.log(response)
      })
  }

  function handleBirthDateChange(birthDate) {
    setBirthdate(birthDate[0])
  }

  function handleNameChange(event) {
    setName(event.target.value)
  }

  function handleSurnameChange(event) {
    setSurname(event.target.value)
  }

  function renderDatePickerInput(props, ref) {
    return (
      <Form.Group controlId="birthDate">
        <Form.Label>Birth date</Form.Label>
        <Form.Control
          ref={ref}
          type="text"
          placeholder="Select your birth date"
          readOnly
          {...props}
        />
      </Form.Group>
    )
  }

  const birthDateMoment = moment(birthDate || undefined)
  const age = moment().diff(birthDateMoment, 'years')

  return (
    <Form onSubmit={handleFormSubmit} className="w-25 mt-5 mx-auto">
      <Form.Group controlId="firstName">
        <Form.Label>First name</Form.Label>
        <Form.Control
          type="text"
          value={name}
          onChange={handleNameChange}
          placeholder="Enter your first name"
        />
      </Form.Group>
      <Form.Group controlId="lastName">
        <Form.Label>Last name</Form.Label>
        <Form.Control
          type="text"
          value={surname}
          onChange={handleSurnameChange}
          placeholder="Enter your last name"
        />
      </Form.Group>
      <Flatpickr
        value={birthDate}
        onChange={handleBirthDateChange}
        render={renderDatePickerInput}
      />
      <Button block variant="primary" type="submit">
        Submit
      </Button>
      <h3 className="mt-5">Your age is: {age}</h3>
    </Form>
  )
}

export default App
