const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const app = express()

app.use(cors())
app.use(bodyParser.json())

app.post('/users', (req, res) => {
  console.log(req.body)
  res.send({
    success: true,
  })
})

app.listen(8080, () => {
  console.log('App is listening to port 8080')
})
